import RPi.GPIO as GPIO
import time
import readchar
import sys, traceback

PIN_PWM_R = 36
PIN_FORWARD_R = 40
PIN_BACKWARD_R = 38

PIN_PWM_L = 33
PIN_FORWARD_L = 35
PIN_BACKWARD_L = 37
SPEED = 80

GPIO.setmode(GPIO.BCM)

GPIO.setup(PIN_PWM_R, GPIO.OUT)
GPIO.setup(PIN_FORWARD_R, GPIO.OUT)
GPIO.setup(PIN_BACKWARD_R, GPIO.OUT)

GPIO.setup(PIN_PWM_L, GPIO.OUT)
GPIO.setup(PIN_FORWARD_L, GPIO.OUT)
GPIO.setup(PIN_BACKWARD_L, GPIO.OUT)

pwmR=GPIO.PWM(PIN_PWM_R, 50)
pwmL=GPIO.PWM(PIN_PWM_L, 50)
pwmR.start(0)
pwmL.start(0)

## TEST
GPIO.output(PIN_FORWARD_R, True)
GPIO.output(PIN_BACKWARD_R, False)
GPIO.output(PIN_FORWARD_L, True)
GPIO.output(PIN_BACKWARD_L, False)
pwmR.ChangeDutyCycle(SPEED)
pwmL.ChangeDutyCycle(SPEED)

GPIO.output(PIN_PWM_R, False)
GPIO.output(PIN_PWM_L, False)

GPIO.output(PIN_BACKWARD_R, False)
GPIO.output(PIN_FORWARD_R, False)
GPIO.output(PIN_BACKWARD_L, False)
GPIO.output(PIN_FORWARD_L, False)

# Read a key
key = readchar.readkey()

GPIO.output(PIN_PWM_R, True)
GPIO.output(PIN_PWM_L, True)
time.sleep(2)
pwmR.stop()
pwmL.stop()
#GPIO.cleanup()
